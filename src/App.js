import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Table from './Table.jsx';
import Form from './Form.jsx';



class App extends Component {

  removeCharacter = index => {
    const { characters } = this.state;

    this.setState({
      characters: characters.filter((character, i) => {
        return i !== index;
      })
    });
  }

  handleSubmit = character => {
    this.setState({ characters: [...this.state.characters, character] });
  }

  state = {
    characters: [
      {
        'name': 'ANNA',
        'job': 'star-struck'
      },
      {
        'name': 'VIOLA',
        'job': 'star-struck'
      },
      
      {
        'name': 'GIORGIA',
        'job': "hearth-face"
      },
      {
        'name': 'ROBERTO',
        'job': "pinocchio"
      },
      {
        'name': 'SKY',
        'job': "party"
      },
      {
        'name': 'NONNA',
        'job': "pinocchio"
      },
      {
        'name': 'NONNO',
        'job': "party"
      },


    ]
  }


  render() {


    return (
      <div className="App">
        <header className="App-header">
          
          <h1 className="App-icon ">
            &#128513;
          </h1>
          <Table characterData={this.state.characters}
            removeCharacter={this.removeCharacter} />
          <Form handleSubmit={this.handleSubmit} />
        </header>

      </div>
    );
  }
}

export default App;
